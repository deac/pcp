
class CodeSegment:
    def __init__(self, dom):
        self.code = [code.firstChild.nodeValue for code in dom.getElementsByTagName("Code")]
        self.code = ''.join(self.code)
        return
    def loadingCode(self):
        return self.code

class Property:
    def __init__(self, dom, _object):
        self.name = dom.getElementsByTagName("name")[0].firstChild.nodeValue
        self.type = dom.getElementsByTagName("type")[0].firstChild.nodeValue
        self.object = _object
        if len(dom.getElementsByTagName("get")) == 0:
            self.get = False
        else:
            self.get = True
        try:
            self.defaultValue = dom.getElementsByTagName("defaultValue")[0].firstChild.nodeValue
        except TypeError:
            self.defaultValue = None
        return
    def loadingCode(self):
        return '\t' + self.object + '->' + self.name + ' = table->getValue<' + self.type + '>("' + self.name + '", {' + self.defaultValue + '});\n'
    def headerCode(self):
        return '  ' + self.type + ' ' + self.name + ';\n'
    def getFunctionCode(self):
        return '  const ' + self.type + '& get_' + self.name + '(){return ' + self.name + ';}\n'
    def renderParameter(self):
        ret = '  _window->setProperty("' + self.type + '", "' + self.name + '", compose(' + self.type + '({' + self.defaultValue + '})));\n'
        return ret

class MetaProperty:
    def __init__(self, dom):
        self.name = dom.getElementsByTagName("name")[0].firstChild.nodeValue
        self.type = dom.getElementsByTagName("type")[0].firstChild.nodeValue
        return
    def metaInitCode(self, _component):
        ret = '  ' + _component.name + '* null = nullptr;\n'
        ret += '  _type->requestVariable<' + self.type + '>(&(null->' + self.name + '));\n'
        return ret;
class Factory:
    def __init__(self, dom):
        self.name = dom.attributes["name"].nodeValue
        self.typeProperties = []
        self.instanceProperties = []
        for property in dom.getElementsByTagName("TypeInit")[0].getElementsByTagName("Property"):
            self.typeProperties.append(Property(property, '_data'))
        for property in dom.getElementsByTagName("ObjectInit")[0].getElementsByTagName("Property"):
            self.instanceProperties.append(Property(property, 'this'))
        self.instanceCode = CodeSegment(dom.getElementsByTagName("ObjectInit")[0])
    def registerActionsCode(self, _component):
        return '  DefaultFactories::global()->addFactory<' + _component.name + ', ' + _component.name + '::' + self.name + 'StaticData>();\n'
    def headerCode(self):
        ret = '  static void typeInit(' + self.name + 'StaticData* _data, PropertyBagInterface* _loader);\n'
        ret += '  void instanceInit(' + self.name + 'StaticData* _data, PropertyBagInterface* _params);\n'
        for property in self.instanceProperties:
            ret += property.getFunctionCode()
        return ret
    def sourceCode(self, _component):
        ret = 'void ' + _component.name + '::typeInit(' + self.name + 'StaticData* _data, PropertyBagInterface* _loader)\n{\n'
        ret += '\tTypeTable* table = _loader->getTypeTable();\n'
        for property in self.typeProperties:
            ret += property.loadingCode()
        ret += '}\nvoid ' + _component.name + '::instanceInit(' + self.name + 'StaticData* _data, PropertyBagInterface* _params)\n{\n'
        ret += '\tTypeTable* table = _params->getTypeTable();\n'
        for property in self.instanceProperties:
            ret += property.loadingCode()
        ret += self.instanceCode.loadingCode()
        ret += '\n}\n'
        return ret
    def renderParameters(self, _component):
        ret = ''
        for property in self.instanceProperties:
            ret += property.renderParameter()
        return ret
    def renderTypeParameters(self, _component):
        ret = ''
        for property in self.typeProperties:
            ret += property.renderParameter()
        return ret

class SimpleFactory:
    def __init__(self, dom):
        self.name = dom.attributes["name"].nodeValue
        self.typeProperties = []
        self.instanceProperties = []
        self.instanceCode = CodeSegment(dom.getElementsByTagName("ObjectInit")[0])
        for property in dom.getElementsByTagName("ObjectInit")[0].getElementsByTagName("Property"):
            self.instanceProperties.append(Property(property, 'this'))
    def registerActionsCode(self, _component):
        return '  DefaultFactories::global()->registerSimpleFactory<' + _component.name + '>();\n'
    def headerCode(self):
        ret = '  void init(PropertyBagInterface* _properties);\n'
        for property in self.instanceProperties:
            ret += property.getFunctionCode()
        return ret
    def sourceCode(self, _component):
        ret = 'void ' + _component.name + '::init(PropertyBagInterface* _loader)\n{\n'
        ret += '\tTypeTable* table = _loader->getTypeTable();\n'
        for property in self.instanceProperties:
            ret += property.loadingCode()
        ret += '}\n'
        return ret
    def renderParameters(self, _component):
        ret = ''
        for property in self.instanceProperties:
            ret += property.renderParameter()
        return ret
    
class GameComponent:
    def __init__(self, dom):
        self.name = dom.attributes["name"].nodeValue
        self.factories = []
        self.metaProperties = []
        self.constructorCode = ""
        self.destructorCode = ""
        for factory in dom.getElementsByTagName("Factory"):
            self.factories.append(Factory(factory))
        try:
            for property in dom.getElementsByTagName("MetaInit")[0].getElementsByTagName("Property"):
                self.metaProperties.append(MetaProperty(property))
        except IndexError:
            pass
        if len(dom.getElementsByTagName("SimpleFactory")) != 0:
            factory = dom.getElementsByTagName("SimpleFactory")[0]
            self.factories.append(SimpleFactory(factory))
        return

def loadGameComponent(_buffer):
    from xml.dom.minidom import parseString
    dom = parseString(open(_buffer).read())
    root = dom.getElementsByTagName("Root")[0]
    component = GameComponent(root.getElementsByTagName("GameComponent")[0])
    return component, root
