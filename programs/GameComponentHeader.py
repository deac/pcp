import programs.GameComponentParser

class Program:
    def __init__self(self):
        return
    def execute(self, _buffer, _metaData):
        component, root = programs.GameComponentParser.loadGameComponent(_buffer)
        ret = '#pragma once\n\n#include <Objects/GameComponent.h>\nclass PropertyBagInterface;\nclass PropertyBagWindow;\nclass TypeTable;\n\n'
        for base in root.getElementsByTagName("Implements"):
            ret += '#include <Objects/GameInterfaces/' + base.firstChild.nodeValue + '.h>\n'
        try:
            ret += root.getElementsByTagName("Code")[0].firstChild.nodeValue + '\n'
        except IndexError:
            pass
        ret += 'class ' + component.name + ': public ' + 'TypedGameComponent<' + component.name + '>'
        for base in root.getElementsByTagName("Implements"):
            str = base.firstChild.nodeValue
            ret += ', public ' + str[str.rfind('/')+1:] + '\n'
        ret += '{\n'
        for factory in component.factories:
            ret += '  struct ' + factory.name + 'StaticData\n'
            ret += '  {\n'
            ret += '    static std::string name()\n'
            ret += '    {\n'
            ret += '      return "' + factory.name + '";\n'
            ret += '    }\n'
            for property in factory.typeProperties:
                ret += '  ' + property.headerCode();
            ret += '    static void staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table);\n'
            ret += '  };\n'
            ret += '  static GameComponent* dynamicLoad();\n'
        ret += 'public:\n'
        ret += '  ' + component.name + '();\n'
        ret += '  ~' + component.name + '();\n'
        try:
            ret += root.getElementsByTagName("Code")[1].firstChild.nodeValue + '\n'
        except IndexError:
            pass
        for factory in component.factories:
            ret += factory.headerCode()
        ret += '  static void registerActions(GameComponentType* _type);\n'
        ret += '  static std::string name(){return "' + component.name + '";};\n'
        ret += '  static void staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table);\n'
        ret += 'private:\n'
        if len(component.factories) == 1:
            for property in component.factories[0].instanceProperties:
                ret += property.headerCode()
        else:
           raise str("Need to program instance properties for classes with multiple factories")
        ret += '};\n'
        return ret

def getProgram():
    return Program()
