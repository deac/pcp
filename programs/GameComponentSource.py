import programs.GameComponentParser

class Program:
    def __init__(self):
        return
    def execute(self, _buffer, _metaData):
        component, root = programs.GameComponentParser.loadGameComponent(_buffer) #FIXME remove root
        ret = ''
        ret += '#include <' + _metaData.filenameRoot + '.h>\n'
        ret += '#include <Objects/GameInterfaces/PropertyBagInterface.h>\n'
        ret += '#include <AbstractFactory/PropertyBagWindow.h>\n\n'
        try:
            ret += root.getElementsByTagName("Code")[-1].firstChild.nodeValue
        except IndexError:
            print ("Blargh")

        ret += component.name + '::' + component.name + '()\n{\n' + component.constructorCode + '}\n'
        ret += component.name + '::~' + component.name + '()\n{\n' + component.destructorCode + '}\n'
        for factory in component.factories:
            ret += factory.sourceCode(component)
        ret += '#include <AbstractFactory/DefaultFactories.h>\n'
        ret += 'void ' + component.name + '::registerActions(GameComponentType* _type)\n'
        ret += '{\n'
        for factory in component.factories:
            for base in root.getElementsByTagName("Implements"):
                ret += '  _type->exposeInterface<' + component.name + ', ' + base.firstChild.nodeValue + '>();\n'
            for property in component.metaProperties:
                ret += property.metaInitCode(component)
            ret += factory.registerActionsCode(component)
        ret += '}\n'
        ret += 'void ' + component.name + '::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)\n'
        ret += '{\n'
        for factory in component.factories:
            ret += factory.renderParameters(component)
        ret += '}\n'

        ret += 'void ' + component.name + '::' + component.factories[0].name + 'StaticData::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)\n'
        ret += '{\n'
        for factory in component.factories:
            ret += factory.renderTypeParameters(component)
        ret += '}\n'
		
        ret += 'GameComponent* ' + component.name + '::dynamicLoad()\n'
        ret += '{\n'
        ret += '  return new Factory<' + component.name + ', ' + component.name + '::' + component.factories[0].name + 'StaticData>();\n'
        ret += '}\n'
        return ret
def getProgram():
    return Program()
