"""This module provides a class for accessing all of its submodules.

I need to make this dynamic, so it can search through a list of directories for modules rather than searching through the pseudo __init__ function submodule list"""

from programs import echo, pcp, python, GameComponentSource, GameComponentHeader
import programs

class Programs:
    def __init__(self):
        self.loaded = dict()
        return
    def __getitem__(self, key):
        try:
            return self.loaded[key]
        except KeyError:
            try:
                return programs.__dict__[key].getProgram()
            except ImportError:
                return programs

def getDatabase():
    return Programs()
