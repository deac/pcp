class Segment:
    """Pseudo XML is broken into these"""
    def __init__(self, _name, _parent):
        self.children = []
        self.name = _name;
        self.parent = _parent
		#        self.endStart = -1
    def execute(self, _buffer, _metaData, _programs):
        return _programs[self.name].execute(self.print(_buffer, _metaData, _programs), _metaData)
    #return '<$' + self.name + "$>{" + self.print(_buffer) + "}"

    def print(self, _buffer, _metaData, _programs):
        if len(self.children) == 0:
            return _buffer[self.openEnd:self.closeStart]
        else:
            ret = _buffer[self.openEnd:self.children[0].openStart+1]
            for child in self.children:
                ret += child.execute(_buffer, _metaData, _programs)
                ret += _buffer[self.children[-1].closeEnd:self.endStart]
            return ret

def parseSegments(_buffer):
    pound = 0
    segment = Segment("echo", None)
    segment.openEnd = 0
    segment.endStart = len(_buffer)
    while pound != -1:
        pound = _buffer.find('£', pound+1)
        if pound >= 1 and _buffer[pound-1] == '<':
            if _buffer[pound-2] != '\\':
                openParserEnd = _buffer.find('>', pound)
                segment.children.append(Segment(_buffer[pound+1:openParserEnd], segment))
                segment = segment.children[-1]
                segment.openStart = pound-2
                segment.openEnd = openParserEnd+1
        else:
            if pound >= 2 and _buffer[pound-2:pound] == '</':
                closeParserEnd = _buffer.find('>', pound)
                if segment.name != _buffer[pound+1:closeParserEnd]:
                    raise Exception("Invalid closing brace '" + _buffer[pound+1:closeParserEnd] + "' expecting '" + segment.name + "'")
                segment.closeStart = pound-2
                segment.closeEnd = closeParserEnd+1
                segment = segment.parent
    return segment



def loadPrograms():
    #return getDatabase()
    import programs
    return programs.getDatabase()

def run(_buffer, _metaData, _args):
    """Executes pcp from _buffer"""
    segments = parseSegments(_buffer)
    programs = loadPrograms()
    return segments.execute(_buffer, _metaData, programs)
