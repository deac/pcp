#!/usr/bin/python
""" pcp is a framework for code generation modules"""
import argparse

def main():
    """Executes from command-line arguments"""
    parser = argparse.ArgumentParser(description=__doc__)
    run(parser)

class FileMetaData:
    def __init__(self, _filenameRoot):
        self.filenameRoot = _filenameRoot
def run(_parser):
    """Executes program from given argparse.ArgumentParser

    Parses command lines arguments, opens input file, calls pcp.run and prints result to output file"""
    import sys

    _parser.add_argument('file')

    args = _parser.parse_args()
    if args.file[-4:] != '.pcp':
        raise Exception(args.file + " is not a .pcp file")
    outputFile = open(args.file[:-4], 'w')
    buffer = open(args.file, 'r', encoding='utf-8').read()

    import pcp
    metaData = FileMetaData(args.file[:args.file.index('.')])
    outputFile.write(pcp.run(buffer, metaData, args))
    sys.exit(0)

if __name__ == "__main__":
    main()
